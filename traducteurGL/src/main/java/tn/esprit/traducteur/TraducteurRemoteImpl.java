package tn.esprit.traducteur;

import javax.ejb.EJB;
import javax.ejb.Stateless;

@Stateless
public class TraducteurRemoteImpl implements TraducteurRemote {

	@EJB
	Dictionnaire dictionnaire;
	
	@EJB(beanName="FrEn")
	TraducteurLocal tradFrEn;
	
	@EJB(beanName="EnFr")
	TraducteurLocal tradEnFr;
	
	@Override
	public String traduire(String mot) {
		// TODO Auto-generated method stub
		if (dictionnaire.isFrench(mot)) {
		//	dictionnaire.traduireFrEn(mot);
			return tradFrEn.traduire(mot);
		}
		else if (dictionnaire.isEnglish(mot)) {
		//	dictionnaire.traduireEnFr(mot);
			return tradEnFr.traduire(mot);
		}
		return null;
	}

}
