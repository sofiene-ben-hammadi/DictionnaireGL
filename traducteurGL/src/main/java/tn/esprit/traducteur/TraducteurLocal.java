package tn.esprit.traducteur;

import javax.ejb.Local;

@Local
public interface TraducteurLocal {

	public String traduire(String mot);
}
